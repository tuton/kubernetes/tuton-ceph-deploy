# Tuton Ceph Deployment

## Howto install

### Get a new enough kustomize

We need at least 3.1 of kustomize.
The version included in kubectl 1.15 will not work.

### Setup Ceph storage

On nodes with storage used for Ceph:

* Setup a LVM VG with name `ceph`.
* Setup one LVM LV per disk with name `osd-X`, where X is between 0 and 7.

### Setup node labels

[Node labels]()https://kubernetes.io/docs/concepts/configuration/assign-pod-node/) are used to steer pods to given nodes.
Some are mandatory, as some pods can only run on selected nodes.

* Setup node label `node-role.tuton.email/master` for all nodes that should run Tuton master components.
* Setup node label `node-role.tuton.email/ceph-mon` for all nodes that should run Ceph monitors.  It should be an odd number.  Removing Ceph monitors from nodes later needs manual intervention, so avoid removing this label.
* Setup node label `node-role.tuton.email/ceph-osd-X`, matching the available LVM LV.

### Setup node taints (optional)

[Node taints](https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/) are used to repell pods from given nodes.
Setting them up is optional.

* `node-role.tuton.email/master`
* `node-role.tuton.email/ceph`

