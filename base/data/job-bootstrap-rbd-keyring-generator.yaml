# SPDX-License-Identifier: GPL-3.0-or-later

---
apiVersion: batch/v1
kind: Job
metadata:
  name: ceph-bootstrap-rbd-keyring-generator
spec:
  template:
    spec:
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: node-role.tuton.email/ceph-mon
                operator: Exists
      tolerations:
      - key: node-role.tuton.email/ceph
        operator: Exists
        effect: NoSchedule
      - key: node-role.tuton.email/master
        operator: Exists
        effect: NoSchedule
      - key: node-role.kubernetes.io/etcd
        operator: Exists
        effect: NoExecute
      - key: node-role.kubernetes.io/controlplane
        operator: Exists
        effect: NoSchedule
      - key: node-role.kubernetes.io/master
        operator: Exists
        effect: NoSchedule

      serviceAccount: ceph-data-generator
      restartPolicy: OnFailure
      securityContext:
        runAsNonRoot: true
        runAsUser: 61796

      initContainers:
      - name: gent-key
        image: ceph/ceph:v14
        imagePullPolicy: Always
        securityContext:
          allowPrivilegeEscalation: false
          readOnlyRootFilesystem: true
        command:
        - /opt/ceph/bin/ceph-auth-bootstrap-get
        - bootstrap-rbd
        volumeMounts:
        - name: ceph-bin
          mountPath: /opt/ceph/bin
          readOnly: true
        - name: ceph-etc
          mountPath: /etc/ceph
          readOnly: true
        - name: tmp
          mountPath: /tmp
          readOnly: false

      containers:
      - name: create-secret
        image: registry.gitlab.com/tuton/tuton-container-external-software/deploy
        imagePullPolicy: Always
        securityContext:
          allowPrivilegeEscalation: false
          readOnlyRootFilesystem: true
        command:
        - /opt/ceph/bin/kubectl-apply-secret-ceph-keyring
        - bootstrap-rbd
        volumeMounts:
        - name: ceph-bin
          mountPath: /opt/ceph/bin
          readOnly: true
        - name: tmp
          mountPath: /tmp
          readOnly: false

      volumes:
      - name: ceph-bin
        projected:
          sources:
          - configMap:
              name: ceph-data-bin
          defaultMode: 0555
      - name: ceph-etc
        projected:
          sources:
          - configMap:
              name: ceph-etc
          - secret:
              name: ceph-mon-keyring
          defaultMode: 0444
      - name: tmp
        emptyDir:
          medium: Memory
