# SPDX-License-Identifier: GPL-3.0-or-later

---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: ceph-mgr
spec:
  replicas: 2
  serviceName: ceph-mgr

  template:
    spec:
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: node-role.tuton.email/master
                operator: Exists
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchLabels:
                  app.kubernetes.io/name: ceph-mgr
              topologyKey: kubernetes.io/hostname

      tolerations:
      - key: node-role.tuton.email/ceph
        operator: Exists
        effect: NoSchedule
      - key: node-role.tuton.email/master
        operator: Exists
        effect: NoSchedule
      - key: node-role.kubernetes.io/etcd
        operator: Exists
        effect: NoExecute
      - key: node-role.kubernetes.io/controlplane
        operator: Exists
        effect: NoSchedule
      - key: node-role.kubernetes.io/master
        operator: Exists
        effect: NoSchedule

      securityContext:
        runAsUser: 61796

      initContainers:
      - name: create-key
        image: ceph/ceph:v14
        imagePullPolicy: Always
        securityContext:
          allowPrivilegeEscalation: false
          readOnlyRootFilesystem: true
        command:
        - /opt/ceph/bin/ceph-mgr-create-key
        env:
        - name: MGR_NAME
          valueFrom:
            fieldRef:
              fieldPath: metadata.name
        volumeMounts:
        - name: ceph-bin
          mountPath: /opt/ceph/bin
          readOnly: true
        - name: ceph-etc
          mountPath: /etc/ceph/ceph.conf
          subPath: ceph.conf
          readOnly: true
        - name: ceph-keyring
          mountPath: /etc/ceph/ceph.keyring
          subPath: ceph.keyring
          readOnly: true
        - name: ceph-data
          mountPath: /var/lib/ceph
          readOnly: false

      containers:
      - name: daemon
        image: ceph/ceph:v14
        imagePullPolicy: Always
        securityContext:
          allowPrivilegeEscalation: false
          readOnlyRootFilesystem: true
        command:
        - ceph-mgr
        - -d
        - -i
        - "$(MGR_NAME)"
        - --mgr-data
        - /var/lib/ceph
        env:
        - name: MGR_NAME
          valueFrom:
            fieldRef:
              fieldPath: metadata.name
        ports:
        - name: metrics
          containerPort: 9283
        volumeMounts:
        - name: ceph-bin
          mountPath: /opt/ceph/bin
          readOnly: true
        - name: ceph-etc
          mountPath: /etc/ceph/ceph.conf
          subPath: ceph.conf
          readOnly: true
        - name: ceph-data
          mountPath: /var/lib/ceph
          readOnly: false
        - name: run
          mountPath: /run
          readOnly: false

      volumes:
      - name: ceph-bin
        projected:
          sources:
          - configMap:
              name: ceph-data-bin
          - configMap:
              name: ceph-mgr-bin
          defaultMode: 0555
      - name: ceph-data
        emptyDir:
          medium: Memory
      - name: ceph-etc
        configMap:
          name: ceph-etc
          defaultMode: 0444
      - name: ceph-keyring
        secret:
          secretName: ceph-admin-keyring
      - name: run
        emptyDir:
          medium: Memory

  volumeClaimTemplates: []
